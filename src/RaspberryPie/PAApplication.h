/*
 * PMainApplication.h
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PAAPPLICATION_H_
#define RASPBERRYPIE_PAAPPLICATION_H_

#include "PPie.h"
#include "PSystem.h"
#include "PExitSignal.h"
#include "PBarrier.h"
#include "PAThread.h"
#include "PALoopThread.h"
#include "PBasicThread.h"
#include "PBasicLoopThread.h"
#include "PBasicSyncThread.h"

#include <cstdio>
#include <memory>
#include <mutex>
#include <map>
#include <iostream>

using namespace std;

namespace Pie {


enum PEThreadType {
	BASIC_THREAD,
	BASIC_LOOP_THREAD,
	SYNC_THREAD,
	SYNC_LOOP_THREAD
};

class PAApplication {
private:
	static PAApplication *app;

	deque<function<void()>> runLaterQueue;
	mutex runLaterQueueLock;
	mutex addRunLaterQueueLock;

	map<int, unique_ptr<PAThread>> pieThreads;
	vector<PAThread *> newPieThreads;
	vector<PALoopThread *> loopPieThreads;
	int basicSyncThreads;

	PBarrier _syncStart;
	PBarrier _syncEnd;

	bool running;
	int exitVal;

	int getExit();
	void _stop(int);

	void _runQueue();
	void _runLater(const function<void()> &);

	void _createBasicThread(const function<void()> &);
	void _createBasicLoopThread(const function<void()> &);
	void _createSyncThread(const function<void()> &);
	void _createSyncLoopThread(const function<void()> &);
protected:
	virtual void initialize();
	virtual void loop() = 0;
public:
	static void runLater(const std::function<void()> &);
	static void exit(int);
	static void stop(int);
	static bool isRunning();
	static void syncStart();
	static void syncEnd();
	static void createThread(PEThreadType, const function<void()> &);

	PAApplication(int argc = 0, char **argv = NULL);
	virtual ~PAApplication() = 0;
	int exec();




};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PAAPPLICATION_H_ */
