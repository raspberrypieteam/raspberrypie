/*
 * PBasicThread.h
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PBASICTHREAD_H_
#define RASPBERRYPIE_PBASICTHREAD_H_

#include "PAThread.h"

#include <thread>
#include <functional>

using namespace std;

namespace Pie {

class PBasicThread : public PAThread {
protected:
	void run();
public:
	explicit PBasicThread(const function<void()> &);
	void join();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PBASICTHREAD_H_ */
