/*
 * PArgs.h
 *
 *  Created on: 2. 4. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PARGS_H_
#define RASPBERRYPIE_PARGS_H_

#include "PArg.h"

#include <queue>
#include <memory>
#include <vector>
#include <string>
#include <regex>
#include <cstdint>
#include <iostream>

namespace Pie {

using namespace std;

class PArgs {
private:
	std::vector<std::unique_ptr<PArg>> args;
	std::string appName;

	void addArgDoubleDashed(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &, std::string, std::string);
	void addArgSingleDashedWithValue(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &, char, std::string);
	void addArgSingleDashedWithoutValue(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &, char);
	void addArgUnDashed(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &, std::string);
public:
	PArgs(int argc = 0, char** argv = NULL);
	bool isInitialized();

	class iterator {
	private:
		std::vector<std::unique_ptr<PArg>>::iterator ptr;
	public:
		iterator(std::vector<std::unique_ptr<PArg>>::iterator);
		iterator operator++();
		iterator operator++(int);
		PArg & operator*();
		PArg * operator->();
		bool operator==(const iterator &);
		bool operator!=(const iterator &);
	};

	class const_iterator {
		private:
			std::vector<std::unique_ptr<PArg>>::const_iterator ptr;
		public:
			const_iterator(std::vector<std::unique_ptr<PArg>>::const_iterator);
			const_iterator operator++();
			const_iterator operator++(int);
			const PArg & operator*();
			const PArg * operator->();
			bool operator==(const const_iterator &);
			bool operator!=(const const_iterator &);
		};

	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PARGS_H_ */
