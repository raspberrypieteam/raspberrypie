/*
 * PBasicThread.h
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PBASICLOOPTHREAD_H_
#define RASPBERRYPIE_PBASICLOOPTHREAD_H_

#include "PALoopThread.h"

#include <functional>

using namespace std;

namespace Pie {

class PBasicLoopThread : public PALoopThread {
protected:
	void loop();
public:
	explicit PBasicLoopThread(const function<void()> &);
	~PBasicLoopThread();
	void join();
	void stop();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PBASICLOOPTHREAD_H_ */
