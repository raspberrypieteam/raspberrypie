/*
 * PAThread.h
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_CONCURENCE_PATHREAD_H_
#define RASPBERRYPIE_CONCURENCE_PATHREAD_H_

#include <thread>
#include <functional>
#include <memory>
#include <condition_variable>
#include <mutex>
#include <iostream>

using namespace std;

namespace Pie {

enum PEThreadState {
	NEW,
	RUNNING,
	ENDED
};

class PAThread {
private:
	static int COUNT;
	const int _id;

	mutex mStart;
	condition_variable cvStart;

protected:
	const function<void()> threadFunction;
	PEThreadState _state;
	thread threadInstance;


	void main();
	virtual void run() = 0;
public:

	explicit PAThread(const function<void()> &);
	virtual ~PAThread();
	void start();

	int id();
	PEThreadState state();

	virtual void join() = 0;
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_CONCURENCE_PATHREAD_H_ */
