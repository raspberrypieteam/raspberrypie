/*
 * PArgs.cpp
 *
 *  Created on: 2. 4. 2016
 *      Author: payne
 */

#include "PArgs.h"

namespace Pie {

PArgs::PArgs(int argc, char** argv) {
	if(argc && argv) {
		std::priority_queue<PArg *, std::vector<PArg *>, PArg::comparator> q;

		appName.clear();
		appName.append(argv[0]);

		for(int i = 1; i < argc; i++) {
			if(std::regex_match(argv[i], std::regex("--[a-zA-Z]+(=)(.)*"))) {
				std::string key(strtok(argv[i], "="));
				std::string value(strtok(NULL, "\0"));
				addArgDoubleDashed(q, &(key[2]), value);
			}
			else if(std::regex_match(argv[i], std::regex("-([a-zA-Z])"))) {
				if(i + 1 < argc && std::regex_match(argv[i + 1], std::regex("()|([^-])(.)*"))) {
					addArgSingleDashedWithValue(q, argv[i][1], argv[i+1]);
					i++;
				}
				else {
					addArgSingleDashedWithoutValue(q, argv[i][1]);
				}
			}
			else if(std::regex_match(argv[i], std::regex("-([a-zA-Z])+"))) {
				int j = 1;
				while(argv[i][j] != '\0') {
					addArgSingleDashedWithoutValue(q, argv[i][j]);
					j++;
				}
			}
			else if(std::regex_match(argv[i], std::regex("()|([^-])(.)*"))) {
				addArgUnDashed(q, argv[i]);
			}
			else {
				std::cerr << "WARNING: Programs argument " << i << " is not in Linux standard: '"<< argv[i] << "'!" << std::endl;
			}
		}

		while(!q.empty()) {
			args.push_back(std::unique_ptr<PArg>(q.top()));
			q.pop();
		}
	}
}

void PArgs::addArgDoubleDashed(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &q, std::string key, std::string value) {
	q.push(new PArg(new std::string(key), new std::string(value)));
}

void PArgs::addArgSingleDashedWithValue(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &q, char key, std::string value) {
	q.push(new PArg(new std::string(1, key), new std::string(value)));
}

void PArgs::addArgSingleDashedWithoutValue(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &q, char key) {
	q.push(new PArg(new std::string(1, key), NULL));
}

void PArgs::addArgUnDashed(std::priority_queue<PArg *, std::vector<PArg*>, PArg::comparator> &q, std::string value) {
	q.push(new PArg(NULL, new std::string(value)));
}

bool PArgs::isInitialized() {
	return !appName.empty();
}

PArgs::iterator PArgs::begin() {
	return iterator(args.begin());
}

PArgs::iterator PArgs::end() {
	return iterator(args.end());
}

PArgs::const_iterator PArgs::begin() const {
	return const_iterator(args.begin());
}

PArgs::const_iterator PArgs::end() const {
	return const_iterator(args.end());
}

PArgs::iterator::iterator(std::vector<std::unique_ptr<PArg>>::iterator ptr) {
	this->ptr = ptr;
}

PArgs::iterator PArgs::iterator::operator++() {
	ptr++;
	return *this;
}

PArgs::iterator PArgs::iterator::operator++(int junk) {
	PArgs::iterator i = *this;
	ptr++;
	return i;
}

PArg & PArgs::iterator::operator*() {
	return (*(*ptr).get());
}

PArg * PArgs::iterator::operator->() {
	return (*ptr).get();
}

bool PArgs::iterator::operator==(const PArgs::iterator &rhs) {
	return ptr == rhs.ptr;
}

bool PArgs::iterator::operator!=(const PArgs::iterator &rhs) {
	return ptr != rhs.ptr;
}

PArgs::const_iterator::const_iterator(std::vector<std::unique_ptr<PArg>>::const_iterator ptr) {
	this->ptr = ptr;
}

PArgs::const_iterator PArgs::const_iterator::operator++() {
	ptr++;
	return *this;
}

PArgs::const_iterator PArgs::const_iterator::operator++(int junk) {
	PArgs::const_iterator i = *this;
	ptr++;
	return i;
}

const PArg & PArgs::const_iterator::operator*() {
	return (*(*ptr).get());
}

const PArg * PArgs::const_iterator::operator->() {
	return (*ptr).get();
}

bool PArgs::const_iterator::operator==(const PArgs::const_iterator &rhs) {
	return ptr == rhs.ptr;
}

bool PArgs::const_iterator::operator!=(const PArgs::const_iterator &rhs) {
	return ptr != rhs.ptr;
}

} /* namespace Pie */
