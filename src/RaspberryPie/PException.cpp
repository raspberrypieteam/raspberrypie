/*
 * PAException.cpp
 *
 *  Created on: 17. 8. 2016
 *      Author: payne
 */

#include "PException.h"

namespace Pie {

PException::PException(const char *pretty_function, string &message) :
	at(chrono::system_clock::now()),
	_function(pretty_function),
	_message(message)
{

}

PException::~PException() {

}

chrono::time_point<chrono::system_clock> &PException::time() {
	return at;
}

string &PException::function() {
	return _function;
}
string &PException::message() {
	return _message;
}

string PException::toString() {
	time_t time = chrono::system_clock::to_time_t(at);
	string timeString(ctime(&time));
	timeString.erase(timeString.size() - 1, 1);
	stringstream ss;
	ss << "[" << timeString << "] '" << _function << "': " <<  _message;
	return ss.str();
}

} /* namespace Pie */
