/*
 * PSystem.h
 *
 *  Created on: 11. 4. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PSYSTEM_H_
#define RASPBERRYPIE_PSYSTEM_H_

#include "PVersion.h"

#include <thread>
#include <fstream>
#include <string>
#include <cstring>
#include <iostream>

#include <unistd.h>


using namespace std;

namespace Pie {

enum PERaspberryBoard {
	UNKNOWN_BOARD,
	RASPBERRY_PI_ZERO,
	RASPBERRY_PI_1,
	RASPBERRY_PI_2,
	RASPBERRY_PI_3
};

enum PERaspberryModel {
	UNKNOWN_MODEL,
	MODEL_A,
	MODEL_B
};

enum PERaspberryRevision {
	UNKNOWN_REVISION
};

class PSystem {
private:
	PVersion version;

	thread::id mainThreadId;

	PERaspberryBoard _board;
	PERaspberryModel _model;
	PERaspberryRevision _revision;


	void cpuinfo();
	void print();

	string boardToString();
	string modelToString();
	string revisionToString();


public:
	PSystem();

	bool isRoot();
	bool isMainThread();

};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PSYSTEM_H_ */
