/*
 * PArg.cpp
 *
 *  Created on: 3. 4. 2016
 *      Author: payne
 */

#include "PArg.h"

namespace Pie {

std::uint32_t PArg::COUNT = 0;

PArg::PArg(std::string *key, std::string *value) :
		priority(COUNT)
{
	this->key.reset(key);
	this->value.reset(value);

	if(!key) {
		priority += 0x80000000;
	}
	COUNT++;
}


//bool PArg::operator<(const PArg &rhs) const {
//	return priority > rhs.priority;
//}

std::string *PArg::getKey() const {
	return key.get();
}

std::string *PArg::getValue() const {
	return value.get();
}

std::uint32_t PArg::getPriority() const {
	return priority;
}

bool PArg::comparator::operator()(const PArg* a, const PArg* b) {
	return (a->getPriority() > b->getPriority());
}

} /* namespace Pie */
