/*
 * PSystem.cpp
 *
 *  Created on: 11. 4. 2016
 *      Author: payne
 */

#include "PSystem.h"

namespace Pie {

PSystem::PSystem() :
	version(PIE_VERSION_MAJOR, PIE_VERSION_MINOR, PIE_VERSION_PATCH),
	_board(PERaspberryBoard::UNKNOWN_BOARD),
	_model(PERaspberryModel::UNKNOWN_MODEL),
	_revision(PERaspberryRevision::UNKNOWN_REVISION)
{
	mainThreadId = this_thread::get_id();

	cpuinfo();

	print();
}

void PSystem::cpuinfo() {
	ifstream proc_cpuinfo;
	proc_cpuinfo.open("/proc/cpuinfo");
	string line;
	while(!proc_cpuinfo.eof()) {
		getline(proc_cpuinfo, line);
		//if() regex match revision:* -> revisionResolver(num);
	}
}

void PSystem::print() {
	cout << "=======" << endl
			<< "Application powered by RaspberryPie (version " << version.toString() << ")" << endl
			<< "=======" << endl
			<< "SYSTEM INFO" << endl
			<< "=======" << endl
			<< "Superuser:\t" << (isRoot() ? "YES" : "NO") << endl
			<< "Board:\t\t" << boardToString() << endl
			<< "Model:\t\t" << modelToString() << endl
			<< "Revision:\t" << revisionToString() << endl
			<< "=======" << endl;
}

bool PSystem::isRoot() {
	return (geteuid() == 0);
}

bool PSystem::isMainThread() {
	return (std::this_thread::get_id() == mainThreadId);
}

string PSystem::boardToString() {
	if(_board == PERaspberryBoard::RASPBERRY_PI_ZERO) {
		return "Raspberry Pi Zero";
	}
	else if(_board == PERaspberryBoard::RASPBERRY_PI_1) {
		return "Raspberry Pi 1";
	}
	else if(_board == PERaspberryBoard::RASPBERRY_PI_2) {
		return "Raspberry Pi 2";
	}
	else if(_board == PERaspberryBoard::RASPBERRY_PI_3) {
		return "Raspberry Pi 3";
	}
	else {
		return "UNKNOWN";
	}
}

string PSystem::modelToString() {
	if(_model == PERaspberryModel::MODEL_A) {
		return "Model A";
	}
	else if(_model == PERaspberryModel::MODEL_B) {
		return "Model B";
	}
	else {
		return "UNKNOWN";
	}
}

string PSystem::revisionToString() {
	if(false) {
		return "";
	}
	else {
		return "UNKNOWN";
	}
}

} /* namespace Pie */
