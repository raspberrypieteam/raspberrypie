/*
 * PBcm2836.h
 *
 *  Created on: 17. 8. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PBCM2836_H_
#define RASPBERRYPIE_PBCM2836_H_

#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>

namespace Pie {

using namespace std;

enum PEGpioType {
	INPUT = 0,
	OUTPUT = 1,
	FUNCTION_1 = 4,
	FUNCTION_2 = 5,
	FUNCTION_3 = 7,
	FUNCTION_4 = 3,
	FUNCTION_5 = 2,
	UNKNOWN = 8
};

class PBcm2836 {
private:
	unsigned long addressPheripeties;
	int mem_fd;
	void *map;
	volatile unsigned int *address;
public:
	PBcm2836();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PBCM2836_H_ */
