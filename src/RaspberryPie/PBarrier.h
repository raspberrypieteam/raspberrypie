/*
 * PBarrier.h
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PBARRIER_H_
#define RASPBERRYPIE_PBARRIER_H_

#include <condition_variable>
#include <mutex>
#include <cstdlib>

using namespace std;

namespace Pie {

class PBarrier {
private:
	mutex _mutex;
	condition_variable cv;
	condition_variable left_cv;
	size_t count;
	size_t left;

public:
	explicit PBarrier(size_t count = 0);
	//~PBarrier();
	void wait();
	void reset(size_t);
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PBARRIER_H_ */
