/*
 * PALoopThread.h
 *
 *  Created on: 14. 8. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PALOOPTHREAD_H_
#define RASPBERRYPIE_PALOOPTHREAD_H_

#include "PAThread.h"

#include <functional>

using namespace std;

namespace Pie {

class PALoopThread: public PAThread {
protected:
	void run();
	virtual void loop() = 0;
public:
	explicit PALoopThread(const function<void()> &);
	virtual ~PALoopThread();

	virtual void stop() = 0;
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PALOOPTHREAD_H_ */
