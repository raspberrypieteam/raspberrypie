/*
 * PExitSignal.h
 *
 *  Created on: 11. 4. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PEXITSIGNAL_H_
#define RASPBERRYPIE_PEXITSIGNAL_H_

#include <sstream>

namespace Pie {

class PExitSignal {
private:
	int exit;
public:
	PExitSignal(int exit);
	const char* what() const noexcept;
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PEXITSIGNAL_H_ */
