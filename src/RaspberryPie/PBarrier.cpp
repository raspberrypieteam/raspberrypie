/*
 * PBarrier.cpp
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#include "PBarrier.h"

namespace Pie {

PBarrier::PBarrier(size_t count) :
	_mutex(),
	cv(),
	left_cv()
{
	this->count = count;
	left = count;
}

void PBarrier::wait() {
	unique_lock<mutex> lock(_mutex);
	if(--count == 0) {
		cv.notify_all();
	}
	else {
		cv.wait(lock, [this] { return count == 0; });
	}
	left--;
	left_cv.notify_one();
}

void PBarrier::reset(size_t count) {
	unique_lock<mutex> lock(_mutex);

	if(this->count) {
		cv.wait(lock, [this] { return this->count == 0; });
	}

	if(left) {
		left_cv.wait(lock, [this] { return left == 0; });
	}

	this->count = count;
	this->left = count;
}

} /* namespace Pie */
