/*
 * PLoopSyncThread.cpp
 *
 *  Created on: 22. 6. 2016
 *      Author: payne
 */

#include "PLoopSyncThread.h"

namespace Pie {

PLoopSyncThread::PLoopSyncThread(function<void()> funct) {
	fnct = funct;

}

void PLoopSyncThread::run() {
	ftr = async(fnct);

}

void PLoopSyncThread::join() {
	ftr.get();
}

} /* namespace Pie */
