/*
 * PLoopSyncThread.h
 *
 *  Created on: 22. 6. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PLOOPSYNCTHREAD_H_
#define RASPBERRYPIE_PLOOPSYNCTHREAD_H_

#include <functional>
#include <future>

using namespace std;

namespace Pie {

class PLoopSyncThread {
private:
	function<void()> fnct;
	future<void> ftr;
public:
	PLoopSyncThread(function<void()>);

	void run();
	void join();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PLOOPSYNCTHREAD_H_ */
