/*
 * PBasicSyncThread.h
 *
 *  Created on: 28. 5. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PBASICSYNCTHREAD_H_
#define RASPBERRYPIE_PBASICSYNCTHREAD_H_

#include "PAApplication.h"

#include <functional>
#include <thread>

using namespace std;

namespace Pie {

class PBasicSyncThread: public PAThread {
protected:
	void run();
public:
	explicit PBasicSyncThread(const function<void()> &);
	~PBasicSyncThread();
	void join();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PBASICSYNCTHREAD_H_ */
