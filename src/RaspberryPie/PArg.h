/*
 * PArg.h
 *
 *  Created on: 3. 4. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PARG_H_
#define RASPBERRYPIE_PARG_H_

#include <memory>
#include <string>
#include <cstdint>

#include <iostream>

namespace Pie {

class PArg {
private:
	static std::uint32_t COUNT;
	std::uint32_t priority;

	std::unique_ptr<std::string> key;
	std::unique_ptr<std::string> value;
public:
	class comparator {
	public:
	    bool operator()(const PArg *, const PArg *);
	};

	PArg(std::string *, std::string *);

	std::string *getKey() const;
	std::string *getValue() const;
	std::uint32_t getPriority() const;
};
} /* namespace Pie */

#endif /* RASPBERRYPIE_PARG_H_ */
