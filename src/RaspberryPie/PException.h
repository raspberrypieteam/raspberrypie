/*
 * PAException.h
 *
 *  Created on: 17. 8. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PEXCEPTION_H_
#define RASPBERRYPIE_PEXCEPTION_H_

#include <string>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iostream>

namespace Pie {

using namespace std;

class PException {
protected:
	chrono::time_point<chrono::system_clock> at;
	string _function;
	string _message;
public:
	PException(const char *, string &);
	virtual ~PException();

	chrono::time_point<chrono::system_clock> &time();
	string &function();
	string &message();

	string toString();
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PAEXCEPTION_H_ */
