/*
 * PMainApplication.cpp
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#include "PAApplication.h"

#include <iostream>

using namespace std;

namespace Pie {

PAApplication *PAApplication::app;

PAApplication::PAApplication(int argc, char **argv) {
	running = false;
	exitVal = 0;
	basicSyncThreads = 0;

	if(argc && argv) {
		PPie::initArgs(argc, argv);
	}
}

PAApplication::~PAApplication() {
}


void PAApplication::_runQueue() {
	while(!runLaterQueue.empty()) {
		unique_lock<mutex> lock(runLaterQueueLock);
		runLaterQueue.front()();
		runLaterQueue.pop_front();
	}
}

void PAApplication::_stop(int exit) {
	app->exitVal = exit;
	for(vector<PALoopThread *>::iterator it = app->loopPieThreads.begin();
			it != app->loopPieThreads.end();
			++it)
	{
		(*it)->stop();
	}
	app->running = false;

}

int PAApplication::exec() {
	if(app) {
		//TODO throw PIllegalStateException
	}
	app = this;
	running = true;
	initialize();
	_syncStart.reset(basicSyncThreads + 1);
	try {
		while(running) {
			_runQueue();

			for(vector<PAThread*>::iterator it = newPieThreads.begin(); it != newPieThreads.end(); ++it) {
				(*it)->start();
			}
			newPieThreads.clear();

			_syncEnd.reset(basicSyncThreads + 1);
			_syncStart.wait();

			loop();
			basicSyncThreads = 0;
			_syncStart.reset(basicSyncThreads + 1);
			_syncEnd.wait();
		}

	}
	catch(Pie::PExitSignal &e) {
		std::cout << e.what() << std::endl;
	}
	catch(exception &e) {
		cerr << "ERROR: You forgot to catch some exception!" << endl;
		cerr << e.what() << endl;
	}

	for(map<int, unique_ptr<PAThread>>::iterator it = pieThreads.begin(); it != pieThreads.end(); ++it) {
		it->second->join();
	}
	app = NULL;
	return getExit();
}

void PAApplication::_runLater(const std::function<void()> &x) {
	unique_lock<mutex> lock(addRunLaterQueueLock);
	if(PPie::system().isMainThread()) {
		runLaterQueue.push_back(x);
	}
	else {
		unique_lock<mutex> lock(runLaterQueueLock);
		runLaterQueue.push_back(x);
	}
}

/*static*/ void PAApplication::runLater(const std::function<void()> &x) {
	app->_runLater(x);
}

/*static*/ void PAApplication::exit(int exit) {
	if(PPie::system().isMainThread()) {
		app->_stop(exit);
		throw PExitSignal(exit);
	} else {
		app->_stop(exit);
	}
}

/*static*/ void PAApplication::stop(int exit) {
	app->_stop(exit);
}


/*static*/ bool PAApplication::isRunning() {
	return app->running;
}

/*static*/ void PAApplication::syncStart() {
	app->_syncStart.wait();
}

/*static*/ void PAApplication::syncEnd() {
	app->_syncEnd.wait();

}

/*static*/ void PAApplication::createThread(PEThreadType type, const function<void()> &func) {
	if(type == PEThreadType::BASIC_THREAD) {
		app->_createBasicThread(func);
	}
	else if(type == PEThreadType::BASIC_LOOP_THREAD) {
		app->_createBasicLoopThread(func);
	}
	else if(type == PEThreadType::SYNC_THREAD) {
		app->_createSyncThread(func);
	}
	else if(type == PEThreadType::SYNC_LOOP_THREAD) {
		app->_createSyncLoopThread(func);
	}
	else {
		cerr << "Unknown thread type" << endl;
	}
}


void PAApplication::_createBasicThread(const function<void()> &fnct) {
	PBasicThread *t = new PBasicThread(fnct);
	pieThreads[t->id()].reset(t);
	newPieThreads.push_back(t);
}
void PAApplication::_createBasicLoopThread(const function<void()> &fnct) {
	PBasicLoopThread *t = new PBasicLoopThread(fnct);
	pieThreads[t->id()].reset(t);
	newPieThreads.push_back(t);
	loopPieThreads.push_back(t);
}
void PAApplication::_createSyncThread(const function<void()> &fnct) {
	PBasicSyncThread *t = new PBasicSyncThread(fnct);
	pieThreads[t->id()].reset(t);
	newPieThreads.push_back(t);
	basicSyncThreads++;
}
void PAApplication::_createSyncLoopThread(const function<void()> &fnct) {}

void PAApplication::initialize() {}

int PAApplication::getExit() {
	return exitVal;
}


} /* namespace Pie */
