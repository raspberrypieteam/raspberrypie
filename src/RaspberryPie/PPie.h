/*
 * PPie.h
 *
 *  Created on: 11. 8. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PPIE_H_
#define RASPBERRYPIE_PPIE_H_

#include "PArgs.h"
#include "PSystem.h"

namespace Pie {

class PPie {
private:
	PSystem _system;
	PArgs _args;

	PPie();
	static PPie& getInstance();
public:
	static void initArgs(int, char**);
	static PSystem& system();
	static PArgs& args();

	PPie(PPie const &) = delete;
	void operator=(PPie const &) = delete;
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PPIE_H_ */
