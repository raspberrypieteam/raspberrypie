/*
 * PPie.cpp
 *
 *  Created on: 11. 8. 2016
 *      Author: payne
 */

#include "PPie.h"

namespace Pie {

PPie::PPie() {
}

PPie &PPie::getInstance() {
	static PPie instance;
	return instance;
}

void PPie::initArgs(int argc, char** argv) {
	PPie& pie = PPie::getInstance();
	if(!pie._args.isInitialized()) {
		pie._args = PArgs(argc, argv);
	}
}

PArgs &PPie::args() {
	return PPie::getInstance()._args;
}

PSystem &PPie::system() {
	return PPie::getInstance()._system;
}

} /* namespace Pie */
