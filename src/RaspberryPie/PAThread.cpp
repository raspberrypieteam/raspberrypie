/*
 * PAThread.cpp
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#include "PAThread.h"

namespace Pie {

int PAThread::COUNT = 0;

PAThread::PAThread(const function<void()> &fnct) :
	_id(++COUNT),
	threadFunction(fnct),
	_state(PEThreadState::NEW),
	threadInstance([this] () { main(); })
{
}

PAThread::~PAThread() {
	//join();
}


//void PAThread::join() {
	//threadInstance.join();
//}

int PAThread::id() {
	return _id;
}

PEThreadState PAThread::state() {
	return _state;
}

void PAThread::main() {
	unique_lock<mutex> lock(mStart);
	if(_state == PEThreadState::NEW) {
		cvStart.wait(lock, [this] () { return _state != PEThreadState::NEW; });
	}
	lock.unlock();
	run();
	_state = PEThreadState::ENDED;
}

void PAThread::start() {
	unique_lock<mutex> lock(mStart);
	if(_state == PEThreadState::NEW) {
		_state = PEThreadState::RUNNING;
		cvStart.notify_all();
	}
}


} /* namespace Pie */
