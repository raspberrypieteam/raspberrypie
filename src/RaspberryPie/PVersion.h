/*
 * PVersion.h
 *
 *  Created on: 16. 8. 2016
 *      Author: payne
 */

#ifndef RASPBERRYPIE_PVERSION_H_
#define RASPBERRYPIE_PVERSION_H_

#include <string>
#include <sstream>

using namespace std;

namespace Pie {

class PVersion {
private:
	const unsigned int pMajor;
	const unsigned int pMinor;
	const unsigned int pPatch;
public:
	PVersion(const unsigned int major = 0u, const unsigned int minor = 0u, const unsigned int patch = 0u);

	const unsigned int major();
	const unsigned int minor();
	const unsigned int patch();

	string toString(char delimiter = '.');
};

} /* namespace Pie */

#endif /* RASPBERRYPIE_PVERSION_H_ */
