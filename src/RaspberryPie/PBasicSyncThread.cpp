/*
 * PBasicSyncThread.cpp
 *
 *  Created on: 28. 5. 2016
 *      Author: payne
 */

#include "PBasicSyncThread.h"

namespace Pie {

PBasicSyncThread::PBasicSyncThread(const function<void()> &func) :
		PAThread(func)
{
}

PBasicSyncThread::~PBasicSyncThread() {

}


void PBasicSyncThread::run() {
	PAApplication::syncStart();
	threadFunction();
	PAApplication::syncEnd();
}


void PBasicSyncThread::join() {
	if(_state == PEThreadState::NEW) {
		start();
	}
	if(threadInstance.joinable()) {
		threadInstance.join();
	}
}




} /* namespace Pie */
