/*
 * PBasicThread.cpp
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#include "PBasicLoopThread.h"

namespace Pie {

PBasicLoopThread::PBasicLoopThread(const function<void()> &func) :
		PALoopThread(func)
{
}

PBasicLoopThread::~PBasicLoopThread() {

}

void PBasicLoopThread::loop() {
	threadFunction();
}


void PBasicLoopThread::join() {
	if(_state == PEThreadState::NEW) {
		start();
	}
	if(threadInstance.joinable()) {
		threadInstance.join();
	}
}

void PBasicLoopThread::stop() {
	_state = PEThreadState::ENDED;
}

} /* namespace Pie */
