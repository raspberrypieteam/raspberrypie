/*
 * PBasicThread.cpp
 *
 *  Created on: 27. 5. 2016
 *      Author: payne
 */

#include "PBasicThread.h"

namespace Pie {

PBasicThread::PBasicThread(const function<void()> &func) :
		PAThread(func)
{
}

void PBasicThread::run() {
	threadFunction();
}


void PBasicThread::join() {
	if(_state == PEThreadState::NEW) {
		start();
	}
	if(threadInstance.joinable()) {
		threadInstance.join();
	}
}

} /* namespace Pie */
