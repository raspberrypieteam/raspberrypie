/*
 * PVersion.cpp
 *
 *  Created on: 16. 8. 2016
 *      Author: payne
 */

#include "PVersion.h"

namespace Pie {

PVersion::PVersion(const unsigned int major, const unsigned int minor, const unsigned int patch) :
		pMajor(major),
		pMinor(minor),
		pPatch(patch)
{

}

const unsigned int PVersion::major() {
	return pMajor;
}

const unsigned int PVersion::minor() {
	return pMinor;
}

const unsigned int PVersion::patch() {
	return pPatch;
}

string PVersion::toString(char delimiter) {
	stringstream ss;
	ss << pMajor << delimiter << pMinor << delimiter << pPatch;
	return ss.str();
}

} /* namespace Pie */
