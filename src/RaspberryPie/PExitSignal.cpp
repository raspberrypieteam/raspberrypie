/*
 * PExitSignal.cpp
 *
 *  Created on: 11. 4. 2016
 *      Author: payne
 */

#include "PExitSignal.h"

namespace Pie {

PExitSignal::PExitSignal(int exit) {
	this->exit = exit;
}

const char* PExitSignal::what() const noexcept {
	std::stringstream ss;
	ss << "Application exit with signal " << exit;
	return ss.str().c_str();
}

} /* namespace Pie */
