/*
 * PBcm2836.cpp
 *
 *  Created on: 17. 8. 2016
 *      Author: payne
 */

#include "PBcm2836.h"

namespace Pie {

PBcm2836::PBcm2836() :
		addressPheripeties(0x20000000)
{
	if((mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
		cerr << "Failed to open /dev/mem, try checking parmissions" << endl;
		//TODO throw
	}
	map = mmap(
			NULL,
			4096,
			PROT_READ | PROT_WRITE,
			MAP_SHARED,
			mem_fd,
			addressPheripeties
	);

	if(map == MAP_FAILED) {
		cerr << "Failed to create memmory block mapped on file /dev/mem" << endl;
		//TODO throw
	}

	address = (volatile unsigned int *)map;
}

} /* namespace Pie */
