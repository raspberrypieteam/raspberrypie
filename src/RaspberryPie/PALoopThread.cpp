/*
 * PALoopThread.cpp
 *
 *  Created on: 14. 8. 2016
 *      Author: payne
 */

#include "PALoopThread.h"

namespace Pie {

PALoopThread::PALoopThread(const function<void()> &fnct) :
		PAThread(fnct)
{

}

PALoopThread::~PALoopThread() {

}

void PALoopThread::run() {
	while(_state == PEThreadState::RUNNING) {
		loop();
	}
}

} /* namespace Pie */
